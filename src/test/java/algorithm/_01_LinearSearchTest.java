package algorithm;

import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;

public class _01_LinearSearchTest {

	@Test
	public void search() throws Exception {
		int[] array = {5, 4, 3, 2, 1};
		/*
		프로시저 Linear-Search(a, n, x)
		입력
		* a: 배열
		* n: 검색 대상이 될 배열 a의 요소의 개수
		* x: 찾고자 하는 값
		출력: a[i] = x 를 만족하는 인덱스 i 또는 0이나 음수처럼 유효하지 않은 인덱스를 나타내는 특별한 값 Not-Found
		 */

		assertThat(_01_LinearSearch.search(array, array.length, 3)).isEqualTo(2);
	}
}